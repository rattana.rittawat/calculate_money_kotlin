package com.example.calculatemoney

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.my_cash_rv.view.*

class MyAdapter(var myDataset: ArrayList<itemMyAdapter>, var context: Context)
    : RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

    //setLayout Adapter in RecycleView
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val textView = LayoutInflater.from(context)
            .inflate(R.layout.my_cash_rv, parent ,false)

        return MyViewHolder(textView)
    }

    override fun getItemCount(): Int = myDataset.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.textCash.text = myDataset[position].item
        holder.textNumber.text = myDataset[position].itemNum.toString()
    }

    class MyViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        var textCash = view.txtCash
        var textNumber = view.txtNumber

    }
}
data class itemMyAdapter(var item: String, var itemNum: Int)
