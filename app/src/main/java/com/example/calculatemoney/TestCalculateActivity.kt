package com.example.calculatemoney

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_test_calculate.*

class TestCalculateActivity : AppCompatActivity() {

    var  myItem : ArrayList<itemMyAdapter> = ArrayList()
    var sumTotal : Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_calculate)

        setOnClick()

        //setCalculater()
        my_recycler_view.apply {
            my_recycler_view.layoutManager = LinearLayoutManager(this@TestCalculateActivity)
            my_recycler_view.adapter = MyAdapter(myItem, this@TestCalculateActivity)

        }



    }

    private fun setOnClick() {
        btnSub_testCal.setOnClickListener {
            sumTotal = num1_testCal.text.toString().toInt() - num2_testCal.text.toString().toInt()
            textShowTotal.text = sumTotal.toString()
            Log.d("last sumTotal : ", "$sumTotal")
            setCalculater()

        }
    }

    private fun setCalculater() {
        //val pay = intArrayOf(7)
        var pay : Int = 0
        val numPay = intArrayOf(500, 100, 50, 20, 10, 5, 1)
        for (i in numPay.indices){
            pay = sumTotal / numPay[i]
            sumTotal -= numPay[i] * pay

            myItem.add(itemMyAdapter("ธนบันตร ${numPay[i]}", itemNum = pay))
            Log.d("last pay : ", "$pay")
        }

//        myItem.add(itemMyAdapter("ธนบันตร 500", itemNum = pay))
//        myItem.add(itemMyAdapter("ธนบันตร 100", itemNum = pay))
//        myItem.add(itemMyAdapter("ธนบันตร 50", itemNum = pay))
//        myItem.add(itemMyAdapter("ธนบันตร 20", itemNum = pay))
//        myItem.add(itemMyAdapter("เหรียญ 10", itemNum = pay))
//        myItem.add(itemMyAdapter("เหรียญ 5", itemNum = pay))
//        myItem.add(itemMyAdapter("เหรียญ 1", itemNum = pay))
        /*for (i in 0..9){
            myItem.add(i)
        }*/
        Log.d("last pay : ", "$myItem")
    }


}
